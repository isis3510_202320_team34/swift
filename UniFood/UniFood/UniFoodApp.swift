//
//  UniFoodApp.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 22/09/23.
//

import SwiftUI
import FirebaseCore
import FirebaseAuth



class AppDelegate:  UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        // Configure Firebase
        FirebaseApp.configure()

        // Auth configuration
        Auth.auth().useEmulator(withHost: "127.0.0.1", port: 9099)

        // Configure User Notifications
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if granted {
                print("Permisos para notificaciones concedidos.")
            } else {
                print("Permisos para notificaciones no concedidos.")
            }
        }

        return true
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Handle the notification response if needed
        completionHandler()
    }
}


@main
struct UniFoodApp: App {


    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    @StateObject var authenticationViewModel = AuthenticationViewModel()
    var body: some Scene {
        WindowGroup {
            //Restaurant_List(restaurantListViewModel: RestaurantListViewModel())
            InitialView()
            .environmentObject(authenticationViewModel)

        }
    }
}
