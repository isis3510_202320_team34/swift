//
//  Restaurant.swift
//  GastroAndes
//
//  Created by Gabriela Garcia Cardenas on 21/09/23.
//

// Edited by Juan Sebastian Pinzon on 22/09/23 at 6pm

import Foundation
import SwiftUI
import FirebaseFirestoreSwift

struct Restaurant: Codable, Identifiable{
    
    
    @DocumentID var id: String?
    var name: String
    var category: String
    var address: String

    var isOpen: Bool
    var phone: String
    var description: String
    var schedule: String
    var rate: Double
    var longitude: Double
    var latitude: Double
    
    var menu: [DishMenu]
    
    var imageName: String

    
}
