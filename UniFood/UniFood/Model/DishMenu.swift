//
//  DishMenu.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 2/10/23.
//


import SwiftUI
import FirebaseFirestoreSwift

struct DishMenu: Codable, Identifiable{
    
    var id: String
    var name: String
    var description: String
    var price: Int
    var prepTime: Int
    var imageName: String
    
    var imageDish: Image {
                Image(imageName)
            }
}

