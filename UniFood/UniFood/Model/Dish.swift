//
//  Dish.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 02/10/23.
//



import Foundation
import SwiftUI
import FirebaseFirestoreSwift

struct Dish: Codable, Identifiable{
    
    
    @DocumentID var id: String?
    var name: String
    var description: String
    var prepTime: String

    var price: String

    private var imageName: String

    var mainImage: Image {
                Image(imageName)
            }

    
}
