//
//  User.swift
//  UniFood
//
//  Created by Isabel Carrascal on 3/10/23.
//

import Foundation
import Foundation
import SwiftUI
import FirebaseFirestoreSwift

struct User: Codable, Identifiable {
    @DocumentID var id: String?
    var email: String
    var name: String
    var password: String
    var phone: String
    var username: String
    var lastAccessDate: String
    var registerDate: String
    var foodPreferences: String
    var firebaseUID: String? // Agrega esta propiedad para almacenar el uid de Firebase

    init(id: String, username: String, email: String, foodPreferences: String, phone: String, registerDate: String, lastAccessDate: String, password: String, name: String, firebaseUID: String?) {
        self.id = id
        self.username = username
        self.email = email
        self.lastAccessDate = lastAccessDate
        self.registerDate = registerDate
        self.foodPreferences = foodPreferences
        self.phone = phone
        self.password = password
        self.name = name
        self.firebaseUID = firebaseUID
    }
}

    
    

