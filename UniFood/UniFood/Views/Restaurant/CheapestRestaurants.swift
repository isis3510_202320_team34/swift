//
//  CheapestRestaurants.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 2/10/23.
//

import SwiftUI

struct CheapestRestaurants: View {
    //var restaurantListViewModel: RestaurantListViewModel
    var restaurantViewModels: [RestaurantViewModel] = []
    
    var body: some View {
            List(restaurantViewModels) { restaurantViewModel in
                NavigationLink {
                    RestaurantDetail(restaurantViewModel: restaurantViewModel)
                }label: {
                    RestaurantRow(restaurantViewModel: restaurantViewModel)
                }
            }
            .navigationTitle(" Cheapest Restaurants")
        
    }
}

struct CheapestRestaurants_Previews: PreviewProvider {
    static var previews: some View {
        CheapestRestaurants(restaurantViewModels: RestaurantListViewModel().restaurantViewModels)
    }
}
