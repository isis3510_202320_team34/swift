//
//  RestaurantRow.swift
//  ProjectRestaurants
//
//  Created by Gabriela Garcia Cardenas on 21/09/23.
//

import SwiftUI

struct RestaurantRow: View {
    var restaurantViewModel: RestaurantViewModel
    
    
    var body: some View {
        VStack {
            
            AsyncImage(
                url:URL(string:restaurantViewModel.restaurant.imageName),
                content: { image in
                    image.resizable()
                        .frame(width: 280, height: 200)
                    
                },
                placeholder: {
                    ProgressView()
                }
            )
            
            HStack {
                Text(restaurantViewModel.restaurant.name)
                    .font(.title)
                Spacer()
                Label("iconStar",systemImage: "star")
                    .labelStyle(.iconOnly)
                    .foregroundColor(.yellow)
                Text(String(restaurantViewModel.restaurant.rate))
                    .font(.title3)
                    
            }
            HStack {
                Text(restaurantViewModel.restaurant.address)
                Spacer()
                Text(restaurantViewModel.restaurant.category)
                    .foregroundColor(Color.blue)
                
            }
            .font(.subheadline)
            .foregroundColor(.secondary)
   
        }
    }
}

struct RestaurantRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            RestaurantRow(restaurantViewModel: RestaurantViewModel(restaurant: restaurants[0]))
        }
            .previewLayout(.fixed(width: 180, height: 200))
    }
}
