//
//  RestaurantDetail.swift
//  ProjectRestaurants
//
//  Created by Gabriela Garcia Cardenas on 21/09/23.
//

import SwiftUI

struct RestaurantDetail: View {
    
    var restaurantViewModel: RestaurantViewModel
    var body: some View {
        
       
        
        VStack {

            AsyncImage(
                url:URL(string:restaurantViewModel.restaurant.imageName),
                content: { image in
                    image.resizable()
                        .frame( height: 300)
                        .ignoresSafeArea(edges: .top)


                    
                },
                placeholder: {
                    ProgressView()
                }
            )
            
            VStack (alignment: .leading){
                HStack {
                    Text(restaurantViewModel.restaurant.name)
                        .font(.title)
                        .fontWeight(.bold)
                    Spacer()
                    Label("iconStar",systemImage: "star")
                        .labelStyle(.iconOnly)
                        .foregroundColor(.yellow)
                    Text(String(restaurantViewModel.restaurant.rate))
                        .font(.title3)
                    
                }
                Group {
                    HStack {
                        Text(restaurantViewModel.restaurant.address)
                        Spacer()
                        Text(restaurantViewModel.restaurant.category)
                            .foregroundColor(Color.blue)
                        
                    }
                    
                    HStack {
                        Text("Open daily time at ")
                            .foregroundColor(Color.green)
                        Text(restaurantViewModel.restaurant.schedule)
                    }
                    
                }
                .font(.headline)
                .foregroundColor(.secondary)
                Divider()
                Text(restaurantViewModel.restaurant.description)
                    .font(.title3)
                Divider()
                Text("Menu & Photos")
                    .font(.title)
                
                MenuRow(restaurantViewModel: restaurantViewModel)
                
//                VStack{
//                    HStack{
//                        restaurantViewModel.restaurant.menu1
//                            .resizable()
//                            .frame( width: 150, height: 150)
//                            .padding()
//                        restaurantViewModel.restaurant.menu2
//                            .resizable()
//                            .frame( width: 150, height: 150)
//                        
//                    }
//                    HStack{
//                        restaurantViewModel.restaurant.menu3
//                            .resizable()
//                            .frame( width: 150, height: 150)
//                            .padding()
//                        restaurantViewModel.restaurant.menu4
//                            .resizable()
//                            .frame( width: 150, height: 150)
//                        
//                    }
//                }
                
                
                
                
            }
            .padding()
            
        }
    }
}

struct RestaurantDetail_Previews: PreviewProvider {
    static var previews: some View {
        RestaurantDetail(restaurantViewModel: RestaurantViewModel(restaurant: restaurants[0]))
    }
}
