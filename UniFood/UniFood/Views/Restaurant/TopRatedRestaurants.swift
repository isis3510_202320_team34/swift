//
//  TopRatedRestaurants.swift
//  UniFood
//
//  Created by Isabel Carrascal on 3/10/23.
//

import SwiftUI

struct TopRatedRestaurants: View {
    var restaurantViewModels: [RestaurantViewModel] = []

    // Función para ordenar restaurantes por calificación.
    var sortedByRating: [RestaurantViewModel] {
        restaurantViewModels.sorted(by: {
            $0.restaurant.rate > $1.restaurant.rate
        })
    }

    var body: some View {
        List(sortedByRating) { restaurantViewModel in
            NavigationLink {
                RestaurantDetail(restaurantViewModel: restaurantViewModel)
            } label: {
                RestaurantRow(restaurantViewModel: restaurantViewModel)
            }
        }
        .navigationTitle("Recommended Restaurants")
    }
}

struct TopRatedRestaurants_Previews: PreviewProvider {
    static var previews: some View {
        TopRatedRestaurants(restaurantViewModels: RestaurantListViewModel().restaurantViewModels)
    }
}


