//
//  Restaurant.List.swift
//  ProjectRestaurants
//
//  Created by Gabriela Garcia Cardenas on 21/09/23.
//

import SwiftUI

struct Restaurant_List: View {
    @State private var searchText = ""
    @ObservedObject var restaurantListViewModel: RestaurantListViewModel
    @State private var cheapestRestaurantsPresented = false
    @State private var sortedCheapestViewModels: [RestaurantViewModel] = []
    @State private var recommendedRestaurantsPresented: Bool = false

    @State private var recommendedViewModels: [RestaurantViewModel] = []

    @State private var filterOption: FilterOption = .byRating
    enum FilterOption {
        case byRating
        case byCategory(String)
    }
    
    var body: some View {
            NavigationLink(destination: CheapestRestaurants(restaurantViewModels: sortedCheapestViewModels), isActive: $cheapestRestaurantsPresented) {
                            EmptyView()
                        }
        
            NavigationLink(destination: TopRatedRestaurants(restaurantViewModels: recommendedViewModels), isActive: $recommendedRestaurantsPresented) {
               EmptyView()
           }
        
            Button(action:{
                let sortedViewModels = restaurantListViewModel.sortRestaurantsByPrice(restaurantListViewModel.restaurantViewModels)
                                sortedCheapestViewModels = sortedViewModels
                                cheapestRestaurantsPresented = true
                
            }){
               Text("Cheapest restaurants")
            }
            
        Button(action:{
            let sortedRecommendedModels = restaurantListViewModel.sortRestaurantsByRating(restaurantListViewModel.restaurantViewModels)
                            recommendedViewModels = sortedRecommendedModels
                            recommendedRestaurantsPresented = true
            
        }){
           Text("Recommended restaurants")
        }
        
        Menu("Filter") {
                Button(action: {
                    filterOption = .byRating
                    recommendedViewModels = restaurantListViewModel.filterRestaurantsByRating()
                    recommendedRestaurantsPresented = true
                }) {
                    Text("By Rating")
                }
                
                ForEach(getAllCategories(), id: \.self) { category in
                    Button(action: {
                        filterOption = .byCategory(category)
                        recommendedViewModels = restaurantListViewModel.filterRestaurantsByCategory(category)
                        recommendedRestaurantsPresented = true
                    }) {
                        Text(category)
                    }
                }
            }
        
            List(restaurantListViewModel.restaurantViewModels) { restaurantViewModel in
                NavigationLink {
                    RestaurantDetail(restaurantViewModel: restaurantViewModel)
                }label: {
                    RestaurantRow(restaurantViewModel: restaurantViewModel)
                }
            }
            .navigationTitle("Restaurants")
    }
                    
}
            


func getAllCategories() -> [String] {
    // Esta función debería devolver todas las categorías posibles.
    // Puedes adaptar esto según de dónde obtengas las categorías.
    return ["Latin American", "Burgers", "Asian", "Mexican","Italiana","Colombian Food", "Sushi"] // ejemplo
}

struct Restaurant_List_Previews: PreviewProvider {
    static var previews: some View {
        Restaurant_List(restaurantListViewModel: RestaurantListViewModel())
    }
}
