//
//  MainOptionsView.swift
//  UniFood
//
//  Created by Juan Sebastian Pinzon Roncancio on 22/09/23.
//

import SwiftUI


struct MainOptionsView: View {
    @State private var showRestaurantListView = false
    @State private var showRestaurantMapView = false
    @State private var showProfile = false

    @StateObject var restaurantListViewModel =  RestaurantListViewModel()
    @StateObject var mapViewModel =  MapViewModel()
    

    var body: some View {
                NavigationView {

        ZStack {
            Image("landing")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)

            Rectangle()
                .foregroundColor(.clear)
                .background(LinearGradient(gradient: Gradient(colors: [Color.black.opacity(0.7), Color.black.opacity(0.3)]), startPoint: .top, endPoint: .bottom))
            VStack (spacing: 20){

                
                Button(action: {
                    print("Continue without login")
                    showRestaurantListView = true

                }){
                    Text("Restaurants List")
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .frame(width: 300, height: 50)
                        .background(Color.orange)
                        .cornerRadius(10)
                }
                           // NavigationLink para navegar a NewView si showNewView es true
                NavigationLink(destination: Restaurant_List(restaurantListViewModel: restaurantListViewModel), isActive: $showRestaurantListView) {
                    EmptyView()
                }
                .hidden()
                
                Button(action: {
                    print("Restaurants Map")
                    showRestaurantMapView = true

                }){
                    Text("Restaurants Map")
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .frame(width: 300, height: 50)
                        .background(Color.orange)
                        .cornerRadius(10)
                }
                           // NavigationLink para navegar a NewView si showNewView es true
                NavigationLink(destination: MapView(restaurantListViewModel: restaurantListViewModel,  mapViewModel:mapViewModel), isActive: $showRestaurantMapView) {
                    EmptyView()
                }
                .hidden()
            
                Button(action: {
                    print("View Profile")
                    showProfile = true

                }){
                    Text("View Profile")
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .frame(width: 300, height: 50)
                        .background(Color.orange)
                        .cornerRadius(10)
                }
                           // NavigationLink para navegar a NewView si showNewView es true
                NavigationLink(destination: ProfileView(), isActive: $showProfile) {
                    EmptyView()
                }
                .hidden()
            }.frame(width: UIScreen.main.bounds.width-40).foregroundColor(.white)
            .onAppear {
            mapViewModel.setupProximityDetection( restaurantListViewModel: restaurantListViewModel)
        }
        }
    }
    }
}
struct MainOptionsView_Previews: PreviewProvider {
    static var previews: some View {
        MainOptionsView()
    }
}
