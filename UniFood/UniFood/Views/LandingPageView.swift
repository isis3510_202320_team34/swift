//
//  ContentView.swift
//  ProjectRestaurants
//
//  Created by Juan Sebastian Pinzon on 22/09/23.
//

import SwiftUI

// When this InitialView is touched it will show the LandingPageView and it will be the first view to be shown
// when the app is launched
struct InitialView: View {
    @State private var isTapped = false
    var body: some View {
        ZStack {
            if isTapped {
                LandingPageView()
            } else {
                Image("landing")
                .resizable()
                .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
                    .onTapGesture {
                        withAnimation {
                            isTapped.toggle()
                        }
                    }
                Text("UniFood")
                    .font(.largeTitle)
                    .fontWeight(.heavy)
                    .foregroundColor(Color.black)
                    .multilineTextAlignment(.center)
                    .offset(y:-300)
                    .onTapGesture {
                        withAnimation {
                            isTapped.toggle()
                        }
                    }
            }
        }
    }

}

struct LandingPageView: View {
    @State private var showNewView = false
    @State private var showLoginView = false

    var body: some View {
                NavigationView {

        ZStack {
            Image("landing")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)

            Rectangle()
                .foregroundColor(.clear)
                .background(LinearGradient(gradient: Gradient(colors: [Color.black.opacity(0.7), Color.black.opacity(0.3)]), startPoint: .top, endPoint: .bottom))
            VStack (spacing: 20){
                Text("UniFood")
                .font(.largeTitle)
                .fontWeight(.heavy)
                .offset(x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 60)
                Spacer()
                VStack {
                    Text("Your best option").font(.system(size: 30, design: .monospaced))
                        .fontWeight(.heavy)                        
                    Text("At your fingertips")
                        .font(.system(size: 30, design: .monospaced))
                        .fontWeight(.heavy)    

                }.padding(.vertical, 20)

                Text ("Find the best restaurants near you")
                        .font(.system(size: 15, design: .monospaced))
                        .fontWeight(.semibold)
                        .lineLimit(2)
                        .multilineTextAlignment(.center)

                
                Button(action: {
                    print("Login")
                    showLoginView = true
                }){
                    Text("Login")
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .frame(width: 300, height: 50)
                        .background(Color.orange)
                        .cornerRadius(10)
                }
                
                
                
                NavigationLink(destination: LoginView(), isActive: $showLoginView) {
                    EmptyView()
                }
                .hidden()
                
                Button(action: {
                    print("Continue without login")
                    showNewView = true

                }){
                    Text("Guest")
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .frame(width: 300, height: 50)
                        .background(Color.orange)
                        .cornerRadius(10)
                }
                           // NavigationLink para navegar a NewView si showNewView es true
                NavigationLink(destination: MainOptionsView(), isActive: $showNewView) {
                    EmptyView()
                }
                .hidden()
            
            }.frame(width: UIScreen.main.bounds.width-40).foregroundColor(.white)
        }
    }
    }
}
struct LandingPageView_Previews: PreviewProvider {
    static var previews: some View {
        InitialView()
    }
}
