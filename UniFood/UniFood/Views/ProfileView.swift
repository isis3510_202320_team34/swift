//
//  ProfileView.swift
//  UniFood
//
//  Created by Isabel Carrascal on 22/09/23.
//
import SwiftUI


struct ProfileView: View {
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel

    @State private var showLandingView = false
    var body: some View {
        VStack {
            Text("Mi perfil")
                .font(.largeTitle)
                .fontWeight(.bold)
                .padding(.top, 20)
            
            Image("profile_photo1")
                .resizable()
                .frame(width: 150, height: 150)
                .clipShape(Circle())
                .overlay(
                    Circle().stroke(Color.white, lineWidth: 4)
                )
                .shadow(radius: 7)
                .padding(.top, 20)
            
            if let user = authenticationViewModel.authenticatedUser {
                           // Mostrar el nombre y el correo electrónico del usuario autenticado en el hilo principal
                          
                               Text(user.name)
                                   .font(.title)
                                   .foregroundColor(.primary)

                               Text(user.email)
                                   .font(.subheadline)
                                   .foregroundColor(.pink)
                           }

                           // Resto del código...
                        else {
                           Text("Usuario no autenticado")
                               .font(.headline)
                               .foregroundColor(.primary)
                       }
            
            
            Spacer() // Espacio antes de la cuadrícula
            
            HStack {
                VStack {
                    Text("20")
                        .font(.headline)
                        .foregroundColor(.primary)
                    Text("Reseñas")
                        .font(.subheadline)
                        .foregroundColor(.pink)
                }
                Spacer()
                VStack {
                    Text("10")
                        .font(.headline)
                        .foregroundColor(.primary)
                    Text("Reservas")
                        .font(.subheadline)
                        .foregroundColor(.pink)
                }
                Spacer()
                VStack {
                    Text("5.0")
                        .font(.headline)
                        .foregroundColor(.primary)
                    Text("Calificación")
                        .font(.subheadline)
                        .foregroundColor(.pink)
                }
            }
            
            Spacer() // Espacio antes de la cuadrícula
            Divider() // Separador después de la cuadrícula
            
            VStack(alignment: .leading) {
                Button(action: {
                    // Acción para la opción "Privacidad"
                }) {
                    Text("Privacidad")
                        .font(.headline)
                        .foregroundColor(.primary)
                }
                
                Divider()
                
                Button(action: {
                    // Acción para la opción "Historial de Reservas"
                }) {
                    Text("Historial de Reservas")
                        .font(.headline)
                        .foregroundColor(.primary)
                }
                
                Divider()
                
                Button(action: {
                    // Acción para la opción "Reseñas"
                }) {
                    Text("Reseñas")
                        .font(.headline)
                        .foregroundColor(.primary)
                }
                
                Divider()
                
                Button(action: {
                    // Acción para la opción "Métodos de Pago"
                }) {
                    Text("Métodos de Pago")
                        .font(.headline)
                        .foregroundColor(.primary)
                }
                
                Divider()
                
                Spacer() // Espacio después del menú de opciones
                
                HStack {
                    Spacer()
                    Button(action: {
                        showLandingView = true
                    }) {
                        Text("Cerrar Sesión")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .background(.pink)
                            .cornerRadius(20)
                            .frame(maxWidth: 250)
                    }
                    NavigationLink(destination: LandingPageView(), isActive: $showLandingView) {
                        EmptyView()
                    }
                    .hidden()
                    Spacer()
                }
            }
            
            .padding(.bottom, 20)
        }
        .padding()
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}


