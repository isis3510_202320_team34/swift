//
//  ContentView.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 22/09/23.
//
import SwiftUI

struct ContentView: View {
    @StateObject private var authenticationViewModel = AuthenticationViewModel()

    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
        }
        .padding()
        // Pasa el authenticationViewModel como un objeto observado
        .environmentObject(authenticationViewModel)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
