//
//  DishItem.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 2/10/23.
//

import SwiftUI

struct DishItem: View {
    
    var dish: DishMenu
    var body: some View {
        VStack {
            dish.imageDish
                .resizable()
                .frame(width: 155, height: 155)
                .cornerRadius(5)
                Text(dish.name)
                Text("$" + String(dish.price))
                
        }
    }
    
}

struct DishItem_Previews: PreviewProvider {
    static var dish = RestaurantListViewModel().restaurantViewModels[0].restaurant.menu[0]
    static var previews: some View {        
        DishItem(dish: dish)
    }
}
