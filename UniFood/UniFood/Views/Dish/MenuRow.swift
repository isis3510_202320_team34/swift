//
//  MenuRow.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 2/10/23.
//

import SwiftUI

struct MenuRow: View {
    
    var restaurantViewModel: RestaurantViewModel
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false){
            HStack(alignment: .top, spacing: 0){
                ForEach(restaurantViewModel.restaurant.menu){ dish in
                    DishItem(dish: dish)
                }
            }
        }
    }
}

struct MenuRow_Previews: PreviewProvider {
    static var previews: some View {
        MenuRow(restaurantViewModel: RestaurantViewModel(restaurant: restaurants[0]))
    }
}
