//
//  ForgotPasswordView.swift
//  UniFood
//
//  Created by Isabel Carrascal on 22/09/23.
//

import SwiftUI

struct ForgotPasswordView: View {
    @State private var email: String = ""
    @State private var showLoginView = false
    
    var body: some View {
        VStack {
            Text("Recupera tu contraseña")
                .font(.title)
                .padding(.bottom, 20)
            
            TextField("Correo electrónico", text: $email)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal)
                .padding(.bottom, 20)
            
            Button(action: {
                
            }) {
                Text("Restablecer")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.pink)
                    .cornerRadius(10)
            }
        }
        .padding()
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}

