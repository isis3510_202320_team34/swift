//
//  MapView.swift
//  ProjectRestaurants
//
//  Created by Juan Sebastián Pinzón on 22/09/23.
//

import SwiftUI
import MapKit

struct MapView: View {

    
    @ObservedObject var restaurantListViewModel: RestaurantListViewModel
    @ObservedObject var mapViewModel: MapViewModel
    @State private var selectedRestaurant: RestaurantViewModel? // Rastrea el restaurante seleccionado
    
    var body: some View {
        ZStack{
        Map(coordinateRegion: $mapViewModel.region, showsUserLocation: true, userTrackingMode: .constant(.follow), annotationItems: restaurantListViewModel.restaurantViewModels) { restaurantViewModel in
            MapAnnotation(
                    coordinate: CLLocationCoordinate2D(latitude: restaurantViewModel.restaurant.latitude, longitude: restaurantViewModel.restaurant.longitude),
                    anchorPoint: CGPoint(x: 0.5, y: 0.7)
                ) {
                    VStack{
 
                        Image(systemName: "mappin.circle.fill")
                            .font(.title)
                            .foregroundColor(.red)
                            .onTapGesture {
                                selectedRestaurant = restaurantViewModel
                            }
                    }
                }
            }
        }
        .onAppear {
            mapViewModel.checkIfLocationServicesIsEnabled()

        }
        .sheet(item: $selectedRestaurant) { restaurantViewModel in
            RestaurantDetail(restaurantViewModel: restaurantViewModel)
       }
    }

}




struct RestaurantMap_List_Previews: PreviewProvider {
    static var previews: some View {
        MapView(restaurantListViewModel: RestaurantListViewModel(), 
                mapViewModel: MapViewModel())
    }
}
