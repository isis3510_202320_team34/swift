//
//  LoginView.swift
//  UniFood
//
//  Created by Isabel Carrascal on 22/09/23.
//

import SwiftUI
import Combine

struct LoginView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var isAuthenticated = false
    @State private var showForgotPasswordView = false
    @EnvironmentObject var authenticationViewModel: AuthenticationViewModel// Variable de estado para controlar la autenticación
    // UserRepository
    @State private var showError = false
    
      @ObservedObject var userViewModel = UserViewModel()
      @State private var cancellables: Set<AnyCancellable> = []

    
    var body: some View {
        
        if showError {
                    Text("Usuario o clave inválida")
                        .foregroundColor(.red)
                        .padding(.bottom, 10)
                }
                
        
        NavigationView {
            ZStack {
                Image("login_background")
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                    .opacity(0.8)
                
                VStack {
                    Spacer()
                    Text("UniFood")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding(.bottom, 30)

                    Spacer()
                    TextField("Email", text: $email)
                        .padding()
                        .background(Color.white.opacity(0.9))
                        .cornerRadius(25)
                        .padding(.bottom, 20)
                        .foregroundColor(.black)

                    SecureField("Contraseña", text: $password)
                        .padding()
                        .background(Color.white.opacity(0.9))
                        .cornerRadius(25)
                        .padding(.bottom, 30)
                        .foregroundColor(.black)

                    Button(action: {
                        Task {
                            let success = await authenticationViewModel.authenticateEmailPassword(email: email, password: password)
                                        if success {
                                            isAuthenticated = true
                                        } else {
                                            // Mostrar un mensaje de error
                                            showError = true
                                            print("Error de autenticación: \(authenticationViewModel.errorMessage)")
                                        }
                                    }
                                })  {
                        Text("Iniciar Sesión")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .foregroundColor(.white)
                            .background(.pink)
                            .cornerRadius(20)
                            .frame(maxWidth: 250)
                    }
                    .background(NavigationLink("", destination: MainOptionsView(), isActive: $isAuthenticated))

                    Button(action: {
                        showForgotPasswordView = true
                    }) {
                        Text("Olvidé mi contraseña")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(maxWidth: .infinity)
                            .foregroundColor(.white)
                            .background(.pink)
                            .cornerRadius(20)
                            .frame(maxWidth: 250)
                    }
                    
                    .background(NavigationLink("", destination: ForgotPasswordView(), isActive: $showForgotPasswordView))

                    Spacer()
                }
                .padding()
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
