//
//  UserRepository.swift
//  UniFood
//
//  Created by Isabel Carrascal on 2/10/23.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import Firebase
import Combine

// 2
class UserRepository: ObservableObject {
    // 3
    private let path: String = "users"
    // 4
    private let store = Firestore.firestore()
    
    @Published var users: [User] = []

    
    // 5
    func add(_ user: User) {
        do {
            _ = try store.collection(path).addDocument(from: user)
        } catch {
            fatalError("Unable to add card: \(error.localizedDescription).")
        }
    }
    
    
    init() {
      get()
    }
    
    func get() {
        store.collection(path).addSnapshotListener { (snapshot, error) in
            if let error = error {
                // Manejar el error aquí
                print("Error obteniendo usuarios: \(error.localizedDescription)")
                return
            }
            
            // Si no hay error, procesa los datos del snapshot
            if let snapshot = snapshot {
                self.users = snapshot.documents.compactMap { document in
                    do {
                        // Intenta obtener un objeto User desde los datos del documento
                        let user = try document.data(as: User.self)
                        return user
                    } catch {
                        // Manejar cualquier error de decodificación aquí
                        print("Error al decodificar el documento: \(error.localizedDescription)")
                        return nil
                    }
                }
            } else {
                // El snapshot está vacío o no existe, así que debes manejarlo de acuerdo a tus necesidades.
                self.users = []
            }
            
            // Realiza cualquier acción adicional que desees con los datos (por ejemplo, actualizar la interfaz de usuario)
            print("USUARIOS")
            print(self.users)
        }
    }

    func getUserData(id: String, completion: @escaping (User?, Error?) -> Void) {
        let userRef = store.collection(path).document(id)
        
        userRef.getDocument { (document, error) in
            if let error = error {
                // Manejar el error si ocurre
                completion(nil, error)
            } else if let document = document, document.exists {
                do {
                    // Intenta obtener un objeto User desde los datos del documento
                    let user = try document.data(as: User.self)
                    completion(user, nil)
                } catch {
                    // Manejar cualquier error de decodificación aquí
                    completion(nil, error)
                }
            } else {
                // El documento no existe o no se encontró, puedes manejarlo de acuerdo a tus necesidades.
                completion(nil, nil)
            }
        }
    }


    

   

    

    
}
