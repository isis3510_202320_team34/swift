//
//  RestaurantRepository.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 1/10/23.
//


import FirebaseFirestore
import FirebaseFirestoreSwift
import Combine

class RestaurantRespository: ObservableObject{
    private let path: String = "restaurants"
    
    private let store = Firestore.firestore()
    
    @Published var restaurants: [Restaurant] = []
    
    init() {
      get()
    }
    
    func get() {
        store.collection(path).addSnapshotListener { (snapshot, error) in
            if let error = error {
                // TODO: handle error
                print("Error getting restaurants: \(error.localizedDescription)")
                return
            }
            self.restaurants = snapshot?.documents.compactMap {
                try? $0.data(as: Restaurant.self)
            } ?? []
            print("RESTAURANTS")
            print(self.restaurants)
            
        }
    }
    
    
    
    
    
}
