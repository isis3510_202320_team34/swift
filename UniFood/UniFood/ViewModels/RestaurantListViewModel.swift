//
//  RestaurantListViewModel.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 1/10/23.
//

import Foundation
import Combine

class RestaurantListViewModel: ObservableObject{
    
    @Published var restaurantRepository = RestaurantRespository()
    
    @Published var restaurantViewModels: [RestaurantViewModel] = []
    
    
    
    private var cancellables: Set<AnyCancellable> = []
    
    init() {
      // 1
        restaurantRepository.$restaurants.map { restaurants in
            restaurants.map(RestaurantViewModel.init)
      }
      // 2
      .assign(to: \.restaurantViewModels, on: self)
      // 3
      .store(in: &cancellables)
    }
    
    private func averagePrice(_ restaurant: Restaurant) -> Double {
        return Double(restaurant.menu.map { $0.price }.reduce(0, +))/Double(restaurant.menu.count)
    }

    func sortRestaurantsByPrice(_ viewModels: [RestaurantViewModel]) -> [RestaurantViewModel] {
        let sortedViewModels = viewModels.sorted(by: {
                averagePrice($0.restaurant) < averagePrice($1.restaurant)
            })

            return sortedViewModels
    }

    
    func sortRestaurantsByRating(_ viewModels: [RestaurantViewModel]) -> [RestaurantViewModel] {
        let recommendedViewModels = viewModels.sorted(by: { $0.restaurant.rate > $1.restaurant.rate })

            return recommendedViewModels
    }
    
    func filterRestaurantsByRating() -> [RestaurantViewModel] {
        return self.restaurantViewModels.sorted(by: { $0.restaurant.rate > $1.restaurant.rate })
    }

    func filterRestaurantsByCategory(_ category: String) -> [RestaurantViewModel] {
        return self.restaurantViewModels.filter { $0.restaurant.category == category }
    }

    
}
