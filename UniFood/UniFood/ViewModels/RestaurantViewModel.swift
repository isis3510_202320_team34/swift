//
//  RestaurantViewModel.swift
//  UniFood
//
//  Created by Gabriela Garcia Cardenas on 1/10/23.
//

import Combine

class RestaurantViewModel: ObservableObject, Identifiable{
    
    private let restaurantRepository = RestaurantRespository()
    @Published var restaurant: Restaurant
    
    private var cancellables: Set<AnyCancellable> = []
    var id = ""

    init(restaurant: Restaurant){
        self.restaurant = restaurant
        
        $restaurant
              .compactMap { $0.id }
              .assign(to: \.id, on: self)
              .store(in: &cancellables)
        
        
    }
    
    private func averagePrice(_ restaurant: Restaurant) -> Double {
        return Double(restaurant.menu.map { $0.price }.reduce(0, +))/Double(restaurant.menu.count)
    }
}
