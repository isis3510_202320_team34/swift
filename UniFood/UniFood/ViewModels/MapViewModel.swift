//
//  MapView.swift
//  ProjectRestaurants
//
//  Created by Juan Sebastián Pinzón on 02/10/23.
//

import Combine
import MapKit

enum MapDetails {
    static let startingLocation = CLLocationCoordinate2D(latitude: 4.601657038006083, longitude: -74.06610298216269)
    static let defaultSpan = MKCoordinateSpan(latitudeDelta: 0.15, longitudeDelta: 0.15)
}   

final class MapViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var region = MKCoordinateRegion(
        center: MapDetails.startingLocation,
        span: MapDetails.defaultSpan
    )
    var locationManager: CLLocationManager?
    private var restaurants: [RestaurantViewModel] = [] // La lista de restaurantes
    
    
    
    
    func checkIfLocationServicesIsEnabled() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            
        } else {
            // Show alert letting the user know they have to turn this on.
            print("Location services are not enabled")
        }
    }

    func checkLocationAuthorization() {
        switch locationManager!.authorizationStatus {
        case .authorizedAlways, .authorizedWhenInUse:
            region = MKCoordinateRegion(
                center: locationManager!.location!.coordinate,
                span: MapDetails.defaultSpan
            )
            
        case .denied:
            print("Location services are not enabled") // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
        case .restricted:
            print("Location services are not enabled") // Show an alert letting them know what's up
            break

        @unknown default:
            break
        }
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }

    
    func setupProximityDetection(restaurantListViewModel: RestaurantListViewModel) {
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
        locationManager?.distanceFilter = 20 // Define una distancia mínima en metros para activar la notificación

        // Agregar los restaurantes de la lista a un array local
        restaurants = restaurantListViewModel.restaurantViewModels
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let userLocation = locations.last else { return }

        // Comprueba la proximidad a los restaurantes
        for restaurantViewModel in restaurants {
            let restaurantLocation = CLLocation(latitude: restaurantViewModel.restaurant.latitude, longitude: restaurantViewModel.restaurant.longitude)
            let distance = userLocation.distance(from: restaurantLocation)

            if distance <= 20 { // Cambia 20 por la distancia deseada para activar la notificación
                // Envía una notificación cuando el usuario está cerca del restaurante
                let notification = UNMutableNotificationContent()
                notification.title = "¡Estás cerca de \(restaurantViewModel.restaurant.name)!"
                notification.body = "¡No te pierdas este increíble restaurante!"

                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let request = UNNotificationRequest(identifier: "restaurantProximity", content: notification, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
        }
    }

}
