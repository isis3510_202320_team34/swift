//
//  UserViewModel.swift
//  UniFood
//
//  Created by Isabel Carrascal on 3/10/23.
//

import Foundation
import Combine
import FirebaseAuth
import FirebaseFirestore


class UserViewModel: ObservableObject {
    @Published var user: User?
    


    // Función para obtener los datos del usuario desde Firestore (puedes mantener esta función aquí)
    private func getUserData(id: String, completion: @escaping (User?, Error?) -> Void) {
        // Implementación para obtener datos del usuario desde Firestore
    }
    
    
    // Método para actualizar el firebaseUID en Firestore
      func updateFirebaseUID(for email: String, with firebaseUID: String) {
          // Aquí implementa la lógica para actualizar el firebaseUID en Firestore

          let db = Firestore.firestore()
          
          db.collection("users").whereField("email", isEqualTo: email).getDocuments { (querySnapshot, error) in
              if let error = error {
                  print("Error al buscar el usuario: \(error.localizedDescription)")
                  return
              }

              // Si se encontró un documento que coincide con el correo electrónico
              if let document = querySnapshot?.documents.first {
                  // Actualiza el campo firebaseUID con el nuevo valor
                  db.collection("users").document(document.documentID).updateData(["firebaseUID": firebaseUID]) { error in
                      if let error = error {
                          print("Error al actualizar el firebaseUID: \(error.localizedDescription)")
                      } else {
                          print("firebaseUID actualizado correctamente")
                      }
                  }
              }
          }
      }
}


