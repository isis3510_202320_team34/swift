//
//  AuthenticationViewModel.swift
//  UniFood
//
//  Created by Isabel Carrascal on 3/10/23.
//

import FirebaseAuth
import Combine

class AuthenticationViewModel: ObservableObject {
    @Published var isUserAuthenticated = false
    @Published var authenticationError: Error?
    @Published var user: FirebaseAuth.User?
    @Published var errorMessage: String = ""
    @Published var userViewModel: UserViewModel
    private var cancellables: Set<AnyCancellable> = []
    @Published var authenticatedUser: User?
    
    init() {
           // Inicializa self.user con un valor nulo
           self.user = nil

           // Inicializa self.userViewModel
           self.userViewModel = UserViewModel() // Asegúrate de que UserViewModel tenga un inicializador sin argumentos

           // Verificar si el usuario ya está autenticado al inicio
           Auth.auth().addStateDidChangeListener { [weak self] (_, user) in
               if let user = user {
                   self?.isUserAuthenticated = true
                   self?.user = user // Asigna el usuario autenticado
               } else {
                   self?.isUserAuthenticated = false
               }
           }
       }

    
    func authenticateEmailPassword(email: String, password: String) async -> Bool {
        do {
            let authRes = try await Auth.auth().signIn(withEmail: email, password: password)

            DispatchQueue.main.async {
                self.user = authRes.user
                print("Usuario \(authRes.user.uid) inicio sesión")
                
                // Asigna el uid de Firebase al objeto User
                let firebaseUID = authRes.user.uid
                self.userViewModel.updateFirebaseUID(for: email, with: firebaseUID)
                
                self.isUserAuthenticated = true
            }

            return true
        } catch {
            DispatchQueue.main.async {
                print(error)
                self.errorMessage = error.localizedDescription
                self.isUserAuthenticated = false
            }

            return false
        }
    }



}


 

    
    
